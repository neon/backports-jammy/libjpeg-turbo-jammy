Source: libjpeg-turbo
Priority: optional
Section: graphics
Maintainer: Ubuntu Developers <ubuntu-devel-discuss@lists.ubuntu.com>
XSBC-Original-Maintainer: Ondřej Surý <ondrej@debian.org>
Uploaders: Mike Gabriel <sunweaver@debian.org>
Build-Depends: cmake (>= 2.8.12),
               debhelper-compat (= 13),
#               default-jdk,
               help2man,
#               javahelper,
			   nasm
Rules-Requires-Root: no
Standards-Version: 4.6.2
Homepage: https://www.libjpeg-turbo.org/
Vcs-Git: https://salsa.debian.org/debian/libjpeg-turbo.git
Vcs-Browser: https://salsa.debian.org/debian/libjpeg-turbo/

#Package: libjpeg-dev
#Architecture: any
#Multi-Arch: same
#Section: libdevel
#Depends: libjpeg8-turbo-dev (>= ${source:Version}),
# 	 ${misc:Depends}
#Conflicts: libjpeg62-dev,
#	   libjpeg7-dev,
#	   libjpeg8-dev,
#	   libjpeg9-dev
#Description: Development files for the JPEG library [dummy package]
# This package depends on default Debian implementation of
# libjpeg.so.8 JPEG library.

Package: libjpeg-turbo8-dev
Architecture: any
Section: libdevel
Multi-Arch: same
Depends: libc-dev,
		 libjpeg-turbo8 (= ${binary:Version}),
		 ${misc:Depends}
#Breaks: libjpeg-turbo8-dev (<< 1:2.1.5-2+22.04+jammy)
Conflicts: libjpeg62-dev,
           libjpeg62-turbo-dev,
	       libjpeg7-dev,
#	       libjpeg8-dev,
	       libjpeg9-dev,
	       libjpeg-turbo8-dev (<< 1:2.1.5-2+22.04+jammy),
Replaces: libjpeg62-dev,
          libjpeg62-turbo-dev,
	      libjpeg7-dev,
#	      libjpeg8-dev,
	      libjpeg9-dev,
	      libjpeg-turbo8-dev (<< 1:2.1.5-2+22.04+jammy)
Provides: libjpeg-dev
Description: Development files for the libjpeg-turbo JPEG library
 The libjpeg-turbo JPEG library is a library for handling JPEG files.
 .
 libjpeg-turbo is a JPEG image codec that uses SIMD instructions (MMX,
 SSE2, NEON) to accelerate baseline JPEG compression and decompression
 on x86, x86-64, and ARM systems.  The libjpeg-turbo JPEG library is
 an API/ABI compatible with the IJG JPEG library.
 .
 This package contains the static library, headers and documentation.

Package: libjpeg-turbo8
Architecture: any
Multi-Arch: same
Section: libs
Depends: ${misc:Depends},
      	 ${shlibs:Depends}
#Breaks: libjpeg-turbo8 (<< 1:2.1.5-2+22.04+jammy)
Replaces: libjpeg62-turbo,
		  libjpeg-turbo8 (<< 1:2.1.5-2+22.04+jammy)
Conflicts: libjpeg62-turbo,
		   libjpeg-turbo8 (<< 1:2.1.5-2+22.04+jammy)
Description: libjpeg-turbo JPEG runtime library
 The libjpeg-turbo JPEG library is a library for handling JPEG files.
 .
 libjpeg-turbo is a JPEG image codec that uses SIMD instructions (MMX,
 SSE2, NEON) to accelerate baseline JPEG compression and decompression
 on x86, x86-64, and ARM systems.  The libjpeg-turbo JPEG library is
 an API/ABI compatible with the IJG JPEG library.
 .
 This package contains the shared runtime library.

Package: libturbojpeg
Architecture: any
Multi-Arch: same
Section: libs
Pre-Depends: ${misc:Pre-Depends}
Depends: ${shlibs:Depends},
		 ${misc:Depends}
#Breaks: libturbojpeg (<< 1:2.1.5-2+22.04+jammy)
Replaces: libturbojpeg0,
		  libturbojpeg (<< 1:2.1.5-2+22.04+jammy)
Conflicts: libturbojpeg0,
		   libturbojpeg (<< 1:2.1.5-2+22.04+jammy),
Description: TurboJPEG runtime library - SIMD optimized
 The libjpeg-turbo JPEG library is a library for handling JPEG files.
 .
 libjpeg-turbo is a JPEG image codec that uses SIMD instructions (MMX,
 SSE2, NEON) to accelerate baseline JPEG compression and decompression
 on x86, x86-64, and ARM systems.
 .
 This package contains the TurboJPEG shared runtime library.

Package: libturbojpeg0-dev
Architecture: any
Section: libdevel
Multi-Arch: same
Depends: libturbojpeg (= ${binary:Version}),
		 ${misc:Depends},
#Breaks: libturbojpeg0-dev (<< 1:2.1.5-2+22.04+jammy),
Replaces: libturbojpeg-dev,
		  libturbojpeg0-dev (<< 1:2.1.5-2+22.04+jammy),
Conflicts: libturbojpeg-dev,
		   libturbojpeg1-dev,
		   libturbojpeg0-dev (<< 1:2.1.5-2+22.04+jammy),
Provides: libturbojpeg-dev
Description: Development files for the TurboJPEG library
 The libjpeg-turbo JPEG library is a library for handling JPEG files.
 .
 libjpeg-turbo is a JPEG image codec that uses SIMD instructions (MMX,
 SSE2, NEON) to accelerate baseline JPEG compression and decompression
 on x86, x86-64, and ARM systems.
 .
 This package contains the static library, headers and documentation for
 the TurboJPEG library.

Package: libjpeg-turbo-progs
Architecture: any
Depends: ${misc:Depends},
		 ${shlibs:Depends},
Conflicts: libjpeg-progs,
		   libjpeg-turbo-progs (<< 1:2.1.5-2+22.04+jammy)
Breaks: libjpeg-turbo-test (<< 2.1.2-0ubuntu1),
#		libjpeg-turbo-progs (<< 1:2.1.5-2+22.04+jammy)
Replaces: libjpeg-progs (<< 8c-2ubuntu5),
          libjpeg-turbo-test (<< 2.1.2-0ubuntu1),
          libjpeg-turbo-progs (<< 1:2.1.5-2+22.04+jammy)
Provides: libjpeg-progs
Description: Programs for manipulating JPEG files
 This package contains programs for manipulating JPEG files from the
 libjpeg-turbo JPEG library:
  * cjpeg/djpeg: convert to/from the JPEG file format
  * rdjpgcom/wrjpgcom: read/write comments in JPEG files
  * jpegtran: lossless transformations of JPEG files
  * jpegexiforient/exifautotran: manipulate EXIF orientation tag
  * tjbench: a simple JPEG benchmarking tool

#Package: libturbojpeg-java
##Architecture: all
#Section: java
#Build-Profiles: <cross !nojava>
#Depends: ${misc:Depends},
#         ${shlibs:Depends},
#         ${java:Depends},
#         libturbojpeg (>= ${source:Version})
#Description: Java bindings for the TurboJPEG library
# The libjpeg-turbo JPEG library is a library for handling JPEG files.
# .
# libjpeg-turbo is a JPEG image codec that uses SIMD instructions (MMX,
# SSE2, NEON) to accelerate baseline JPEG compression and decompression
# on x86, x86-64, and ARM systems.
# .
# This package contains the Java bindings.
